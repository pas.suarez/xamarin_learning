﻿namespace Xamarin_learning.Views
{
    using System;
    using Xamarin.Forms;

    public partial class LandsPage : ContentPage
    {
        public LandsPage()
        {
            InitializeComponent();

        }

        private void Ladder_NO_Button_Clicked(object sender, EventArgs e)
        {
            if (Ladder_NC_Button.IsEnabled == true) { Ladder_NC_Button.IsEnabled = false; }
            else { Ladder_NC_Button.IsEnabled = true; }

            DisplayAlert("Ladder_Buttons", "NO Simbol Pressed", "OK");
        }

        private void Ladder_NC_Button_Clicked(object sender, EventArgs e)
        {

        }
    }
}